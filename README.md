# Grafana authenticating datasource proxy
A **very** simple proxy server intended to add authentication
to Grafana datasources.

It uses the session cookie to identify which Grafana user is
making the request via `http://[Grafana endpoint]/api/user`.
Only requests of users on the `allowed_users` are let through.

Additionally provides basic auth and basic read/write permissions
for configured path prefixes.

Proxy code adapted from
https://gist.github.com/yowu/f7dc34bd4736a65ff28d



## Configuration
Included is a config template for Grafana Loki.

#### `bindaddr`
Public facing address to bind.
Can use `${HOSTNAME}` to bind on the public interface only.

#### `proxyaddr`
Local address of proxied datasource.

#### `users`
User info for basic auth.

Generate hash and salt with
`grafana-datasource-proxy -hashsalt password`.

+ `username`: Username.
+ `passwordhash`: Password hash.
+ `passwordsalt`: Salt for the password hash.
+ `permissions`: List of permissions.
+ `require_session`: If true, after basic auth perform
  additional authentication against Grafana endpoint
  with `grafana_session` cookie

Permission names must match those configured in the global
`permissions` section.

#### `permissions`
Allowlist for users with read/write permissions per HTTP
method and path prefix.

Separate maps for HTTP methods like GET, POST:
```
"permissions": {
  "read": {
    "GET": ["/path1", "/path2"],
    "POST": []
  },
  "write": {
    "GET": [],
    "POST": ["/path1", "/path2"]
  }
}
```
You can add custom permission types (e.g., "metrics")
and additional methods like PUT, HEAD, ... as required.

**Important:** Prefixes that are not listed are never allowed!

#### `grafana_endpoints`
Map of IP addresses to possible resolved endpoint addresses.
For each IP include all valid addresses that a user could use.
Required since the Grafana endpoint will only give information
for the same domain from where the user logged in.
And I can't be bothered to find out how to get all know hostnames
in Go right now.

#### `allowed_users`
List of allowed Grafana users.
Checked when user has `require_session=true`.



## Installation
Create `config.json`, then run `install.sh` in the same directory.



## Enable service in systemd
On the Loki node:
```
systemctl daemon-reload
systemctl enable grafana-datasource-proxy.service
systemctl start grafana-datasource-proxy.service
```

module grafana-datasource-proxy

go 1.16

require (
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a
	golang.org/x/sys v0.0.0-20210603125802-9665404d3644 // indirect
)

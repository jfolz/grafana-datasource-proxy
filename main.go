package main

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	sha3 "golang.org/x/crypto/sha3"
)

var (
	// BuildVersion program version
	BuildVersion string = "1.0.0"
	// BuildTime time of build
	BuildTime string = "none"
)

func Contains(arr []string, v string) bool {
	for _, a := range arr {
		if a == v {
			return true
		}
	}
	return false
}

func GenerateSalt() string {
	b := make([]byte, 64)
	rand.Read(b)
	return base64.StdEncoding.EncodeToString(b)
}

func HashPassword(password string, salt string) string {
	digest := sha3.Sum512([]byte(password + salt))
	return base64.StdEncoding.EncodeToString(digest[:])
}

// Hop-by-hop headers. These are removed when sent to the backend.
// http://www.w3.org/Protocols/rfc2616/rfc2616-sec13.html
var hopHeaders = []string{
	"Connection",
	"Keep-Alive",
	"Proxy-Authenticate",
	"Proxy-Authorization",
	"Te",
	"Trailers",
	"Transfer-Encoding",
	"Upgrade",
}

func CopyHeader(dst, src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			dst.Add(k, v)
		}
	}
}

func DelHopHeaders(header http.Header) {
	for _, h := range hopHeaders {
		header.Del(h)
	}
}

func AppendHostToXForwardHeader(header http.Header, host string) {
	if prior, ok := header["X-Forwarded-For"]; ok {
		host = strings.Join(prior, ", ") + ", " + host
	}
	header.Set("X-Forwarded-For", host)
}

type UserData struct {
	Username       string   `json:"username"`
	PasswordHash   string   `json:"passwordhash"`
	PasswordSalt   string   `json:"passwordsalt"`
	Permissions    []string `json:"permissions"`
	RequireSession bool     `json:"require_session"`
}

type Permissions = map[string][]string

type Config struct {
	BindAddr         string                 `json:"bindaddr"`
	ProxyAddr        string                 `json:"proxyaddr"`
	Users            map[string]UserData    `json:"users"`
	Permissions      map[string]Permissions `json:"permissions"`
	GrafanaEndpoints map[string][]string    `json:"grafana_endpoints"`
	AllowedUsers     []string               `json:"allowed_users"`
}

type GrafanaUser struct {
	Login          string `json:"login"`
	IsDisabled     bool   `json:"isDisabled"`
	IsGrafanaAdmin bool   `json:"isGrafanaAdmin"`
}

type AuthProxy struct {
	Client    *http.Client
	Config    Config
	AuthCache map[string]time.Time
}

// Clean up auth cache
func (p *AuthProxy) RemoveExpiredTokens() {
	now := time.Now()
	for token, expires := range p.AuthCache {
		if expires.Before(now) {
			delete(p.AuthCache, token)
		}
	}
}

// Query grafana/api/user to identify user token
func (p *AuthProxy) IdentifyUser(host string, token *http.Cookie) GrafanaUser {
	userinfo := GrafanaUser{}

	log.Info("Check grafana_session token with " + host)
	authreq, err := http.NewRequest("GET", "http://"+host+"/api/user", nil)
	if err != nil {
		log.Error(err)
		return userinfo
	}

	authreq.Header.Add("Accept", "application/json")
	authreq.AddCookie(token)
	resp, err := p.Client.Do(authreq)
	if err != nil {
		log.Error(err)
		return userinfo
	}
	defer resp.Body.Close()
	jsontext, _ := io.ReadAll(resp.Body)
	log.Info("Response: " + string(jsontext))
	json.Unmarshal(jsontext, &userinfo)

	return userinfo
}

func (p *AuthProxy) CheckBasicAuth(req *http.Request) (UserData, error) {
	if user, pass, ok := req.BasicAuth(); ok {
		if info, ok := p.Config.Users[user]; ok && user == info.Username {
			hash := HashPassword(pass, info.PasswordSalt)
			if hash == info.PasswordHash {
				return info, nil
			}
		}
	}
	return UserData{}, errors.New("basic auth failed")
}

func (p *AuthProxy) CheckPermissions(req *http.Request, user UserData) bool {
	var prefixes []string
	for _, perm := range user.Permissions {
		if methods, ok := p.Config.Permissions[perm]; ok {
			if ps, ok := methods[req.Method]; ok {
				prefixes = append(prefixes, ps...)
			}
		}
	}
	path := req.URL.String()
	for _, prefix := range prefixes {
		if strings.HasPrefix(path, prefix) {
			log.Debug("Path matches allowed prefix " + prefix)
			return true
		}
	}
	log.Debug("No matching prefix found for path " + path)
	return false
}

func (p *AuthProxy) CheckAuthentication(req *http.Request) error {
	// Clear expired tokens from auth cache
	p.RemoveExpiredTokens()

	// Check basic auth username password & permissions
	if userdata, err := p.CheckBasicAuth(req); err != nil {
		return err
	} else if !p.CheckPermissions(req, userdata) {
		return errors.New("permission denied")
	} else if !userdata.RequireSession {
		// Immediately allow user if Grafana session is not required
		return nil
	}

	// Get Grafana session cookie
	token, err := req.Cookie("grafana_session")
	if err != nil {
		return err
	}
	if token == nil || token.Value == "" {
		return errors.New("grafana_session cookie not set or empty")
	}

	// User token is in the auth cache and has not expired
	if expires, ok := p.AuthCache[token.Value]; ok {
		if expires.After(time.Now()) {
			return nil
		} else {
			log.Debug("Authentication has expired")
		}
	}

	// Query Grafana API for user info using token from cookie
	var userinfo GrafanaUser
	ipaddr, _, err := net.SplitHostPort(req.RemoteAddr)
	if err != nil {
		return errors.New("cannot parse remote address")
	}
	if hostnames, ok := p.Config.GrafanaEndpoints[ipaddr]; ok {
		for _, hostname := range hostnames {
			userinfo = p.IdentifyUser(hostname, token)
			if userinfo.Login != "" {
				break
			}
		}
	} else {
		return errors.New("not a known Grafana endpoint")
	}

	// Check if login is valid
	if userinfo.IsDisabled {
		return errors.New(userinfo.Login + " is disabled")
	} else if userinfo.IsGrafanaAdmin {
		log.Info(userinfo.Login, " is Grafana admin")
	} else if !Contains(p.Config.AllowedUsers, userinfo.Login) {
		return errors.New(userinfo.Login + " is not in allowed_users")
	} else {
		log.Info(userinfo.Login, " is in allowed_users")
	}

	// Add token to auth cache and epxire in 24 hours
	expires := time.Now().Add(24 * time.Hour)
	p.AuthCache[token.Value] = expires
	log.Debug("Token of " + userinfo.Login + " added to auth cache, expires after " + expires.Format("2006-01-02 15:04:05"))

	// User is allowed, so return no error
	return nil
}

func (p *AuthProxy) ServeHTTP(wr http.ResponseWriter, req *http.Request) {
	log.Debug(req.RemoteAddr, " ", req.Method, " ", req.URL)

	// Verify that request comes from an allowed user
	if err := p.CheckAuthentication(req); err != nil {
		log.Warn("Authentication failed: ", err)
		http.Error(wr, "authentication failed", http.StatusForbidden)
		return
	}

	// Prepare request to be sent to proxied host
	proxyreq, err := http.NewRequest(req.Method, "http://"+p.Config.ProxyAddr+req.URL.String(), req.Body)
	if err != nil {
		http.Error(wr, "Server Error", http.StatusInternalServerError)
		log.Fatal("ServeHTTP:", err)
	}
	DelHopHeaders(req.Header)
	CopyHeader(proxyreq.Header, req.Header)

	if clientIP, _, err := net.SplitHostPort(req.RemoteAddr); err == nil {
		AppendHostToXForwardHeader(proxyreq.Header, clientIP)
	}

	// Make request to proxied address
	resp, err := p.Client.Do(proxyreq)
	if err != nil {
		http.Error(wr, "Server Error", http.StatusInternalServerError)
		log.Fatal("ServeHTTP:", err)
	}
	defer resp.Body.Close()

	log.Debug(req.RemoteAddr, " ", resp.Status)

	// Respond with answer of proxy
	DelHopHeaders(resp.Header)
	CopyHeader(wr.Header(), resp.Header)
	wr.WriteHeader(resp.StatusCode)
	if _, err := io.Copy(wr, resp.Body); err != nil {
		log.Error("io.Copy: ", err)
	}
}

func NewAuthProxy(config Config) *AuthProxy {
	p := new(AuthProxy)
	p.Client = new(http.Client)
	p.Config = config
	p.AuthCache = make(map[string]time.Time)
	return p
}

func LoadConfig(configpath string) (Config, error) {
	f, err := os.Open(configpath)
	if err != nil {
		return Config{}, err
	}
	defer f.Close()
	config := Config{}
	json.NewDecoder(f).Decode(&config)
	hostname, err := os.Hostname()
	if err != nil {
		return Config{}, err
	}
	config.BindAddr = strings.Replace(config.BindAddr, "${HOSTNAME}", hostname, -1)
	return config, nil
}

func main() {
	// Setup the logger
	log.SetOutput(os.Stdout)
	formatter := new(log.TextFormatter)
	formatter.DisableTimestamp = true
	formatter.DisableLevelTruncation = true
	formatter.PadLevelText = true
	log.SetFormatter(formatter)

	// Parse command line flags and more logger setup
	var version = flag.Bool("version", false, "Print version info.")
	var configpath = flag.String("config", "config.json", "Path to config file.")
	var loglevel = flag.String("loglevel", "info", "Log level (panic, fatal, error, warn, info, debug, trace).")
	var hashsalt = flag.String("hashsalt", "", "Prints hash and salt for given password.")
	flag.Parse()

	if *version {
		fmt.Println(BuildVersion, BuildTime)
		return
	}

	if *hashsalt != "" {
		salt := GenerateSalt()
		hash := HashPassword(*hashsalt, salt)
		fmt.Println("Hash:", hash)
		fmt.Println("Salt:", salt)
		return
	}

	level, err := log.ParseLevel(*loglevel)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(level)

	// Load the config file
	config, err := LoadConfig(*configpath)
	if err != nil {
		log.Fatal("Cannot open config file ", *configpath, ": ", err)
		return
	}

	// Start the actual proxy
	handler := NewAuthProxy(config)
	log.Info("Starting proxy server on ", config.BindAddr)
	if err := http.ListenAndServe(config.BindAddr, handler); err != nil {
		log.Fatal("ListenAndServe: ", err)
		return
	}
}

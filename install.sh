#!/bin/bash
set -e
set -u


PROXY_VERSION="${PROXY_VERSION:-1.2.0}"


ROOT_DIR="$(dirname "$0")"
ROOT_DIR="$(readlink -f "${ROOT_DIR}")"


BINARY_URL="https://gitlab.com/jfolz/grafana-datasource-proxy/-/jobs/artifacts/${PROXY_VERSION}/raw/grafana-datasource-proxy?job=release"
BINARY_PATH="grafana-datasource-proxy-${PROXY_VERSION}"


if [[ ! -f config.json ]]; then
    echo "Please create config.json first:"
    echo "cp etc/loki-config.json.example config.json"
    echo "Then fill in user details etc."
    exit 1
fi

if [ ! -f "${BINARY_PATH}" ]; then
	curl -L "${BINARY_URL}" -o "${BINARY_PATH}"
fi

chmod a+x "${BINARY_PATH}"

mkdir -p /etc/grafana-datasource-proxy

cp config.json /etc/grafana-datasource-proxy/
cp "${BINARY_PATH}" /usr/local/bin/grafana-datasource-proxy
cp "${ROOT_DIR}"/systemd/grafana-datasource-proxy.service /etc/systemd/system/

chmod -R go-rw /etc/grafana-datasource-proxy
